// Soil One - Receiver

#include <RH_ASK.h>

RH_ASK driver;

void setup()
{
  Serial.begin(9600);
  // RH_ASK(2000,11,12,10,false);
  if (!driver.init()){
    Serial.println("init failed");
  }
}

void loop()
{
    uint8_t buf[RH_ASK_MAX_MESSAGE_LEN];
    uint8_t buflen = sizeof(buf);
    uint16_t receivedValue;

    if (driver.recv(buf, &buflen)){ // Non-blocking
      Serial.print("length,");
      Serial.println(buflen);
      if(buflen == 2)
      {
         uint16_t receivedValue = buf[0] | uint16_t(buf[1]) << 8;

         Serial.print("Value,");
         Serial.println(receivedValue);
      }
    }
  
    delay(500);
}
