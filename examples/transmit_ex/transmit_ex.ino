/*
 * RF433 MHz Tx code
 * this code for turn on and LED on receiver side
 * every 2 seconds
 * connect the Data pin to pin 12
 * written By: Mohannad Rawashdeh
 * 13 Feb 2017
 */
#include <RH_ASK.h>

RH_ASK potato;

const int LEDpin=13;// indicator pin
const int delayTime = 2000;
const char *On_command = "123";
const char *Off_command = "125";

void setup()
{
  pinMode(LEDpin,OUTPUT);
    while (!potato.init())
    {
  // no RF connected ! or error on initi
      digitalWrite(LEDpin,1);
      delay(100);
      digitalWrite(LEDpin,0);
      delay(100);      
    }
}

void loop()
{
 potato.send((uint8_t *)On_command, strlen(On_command));
 potato.waitPacketSent();
 delay(delayTime);
}
