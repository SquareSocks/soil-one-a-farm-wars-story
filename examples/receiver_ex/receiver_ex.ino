/*<br>RF433 MHz Rx code
this code for turn on and LED on receiver side
every 2 seconds
connect the Data pin to pin 11
written By: Mohannad Rawashdeh
13 Feb 2017
visit my instructables profile for more information
<a href="https://www.instructables.com/member/Mohannad%20Rawashdeh/?publicView=true</p">
https://www.instructables.com/member/Mohannad%20R...</a>><p>
*/

#include <RH_ASK.h>
//#include <spi.h> // Not actually used but needed to compile

RH_ASK Rawashdeh;

const int LEDpin=13;// indicator pin
const int delayTime = 2000;
const char *On_command = "1";
const char *Off_command = "0";

void setup()
{
  Serial.begin(9600);
//  RH_ASK(2000,11,12,10,false);
  pinMode(LEDpin,OUTPUT);
  if (!Rawashdeh.init())
  {
  // no RF connected ! or error on initi    
  digitalWrite(LEDpin,1);
  delay(100);
  digitalWrite(LEDpin,0);
  delay(100);          
  }
}

void loop()
{
    uint8_t buf[RH_ASK_MAX_MESSAGE_LEN];
    uint8_t buflen = sizeof(buf);
    Serial.print("length,");
    Serial.println(buflen);
    for (int i = 0; i < 6; i++)
    {
      Serial.print("byte [");
      Serial.print(i);
      Serial.print("],");
      Serial.println(buf[i]);
      
    }


//    Serial.print(buf[1]);
//    Serial.println(buf[2]);
    if (Rawashdeh.recv(buf, &buflen)) // Non-blocking
    {
      if(buf[0]=='y')
      {
 //       if(buf[1]=='e')
 //       {
 //         if(buf[1]=='s')
 //         {
          digitalWrite(LEDpin,1);        
 //         }
 //       }
      }
      if(buf[0]=='n')
      {
 //       if(buf[1]=='o')
 //       {
        digitalWrite(LEDpin,0);  
 //       }      
      }      
    }
}
