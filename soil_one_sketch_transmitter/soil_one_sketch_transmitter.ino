// Soil One - Transmitter

#include <RH_ASK.h>

RH_ASK driver;

// connect the Data pin from the rf transmitter to pin 12, aka txPin
const int sensorPin = A0; // moisture sensor pin

int sensorValue;
uint16_t castSensorValue;
void setup(){
  Serial.begin(9600); //for debugging only
  if (!driver.init()){
    Serial.println("init failed");
  }
}

void loop(){
  sensorValue = analogRead(sensorPin);
  Serial.println(sensorValue);
  
  castSensorValue = (uint16_t) sensorValue;
  Serial.println(castSensorValue);

  uint8_t lo = castSensorValue & 0xFF;
  uint8_t hi = castSensorValue >> 8;
  uint8_t message[2] = {lo, hi};

  Serial.println(lo);
  Serial.println(hi);
  Serial.println("****");

  driver.send((uint8_t *)message, sizeof(message));
  driver.waitPacketSent();
  
  delay(500); //wait .5 seconds.
}
